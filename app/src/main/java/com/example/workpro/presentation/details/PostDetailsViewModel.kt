package com.example.workpro.presentation.details

import com.example.workpro.domain.models.PostModel
import com.example.workpro.domain.repository.Repository
import com.example.workpro.presentation.details.actions.GoToPostEditAction
import com.zuluft.mvvm.viewModels.BaseViewModel

class PostDetailsViewModel : BaseViewModel<PostDetailsViewState>() {

    override fun getInitialState(): PostDetailsViewState {
        return PostDetailsViewState()
    }

    fun goToEditFragment(model: PostModel) {
        dispatchAction(GoToPostEditAction(model))
    }
}