package com.example.workpro.presentation.actions

import com.example.workpro.presentation.main.MainFragmentViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class DismissLoaderAction(b: Boolean) : ViewStateAction<MainFragmentViewState> {
    override fun newState(oldState: MainFragmentViewState): MainFragmentViewState {
        return oldState.copy(
            dismissLoader = DisposableValue(true)
        )
    }
}