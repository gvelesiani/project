package com.example.workpro.presentation.main

import android.os.Bundle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import com.example.workpro.R
import com.example.workpro.domain.models.PostModel
import com.example.workpro.presentation.adapter.RecyclerViewAdapter
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_main)
class MainFragment : BaseFragment<MainFragmentViewState, MainFragmentViewModel>() {
    private val posts = mutableListOf<PostModel>()
    private lateinit var rvAdapter: RecyclerViewAdapter
    private val viewModel: MainFragmentViewModel by viewModel()

    override fun provideViewModel(): MainFragmentViewModel {
        return viewModel
    }

    override fun reflectState(viewState: MainFragmentViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            ///requireContext().showThrowableMessage(it)
        }

        viewState.posts?.getValue().notNull {
            dismissAndClearDialogs()
            rvAdapter.setPosts(it)
        }
        viewState.dismissLoader?.getValue().notNull {
            dismissAndClearDialogs()
        }

        viewState.goToDetails?.getValue().notNull {
            findNavController().navigate(
                MainFragmentDirections.actionMainFragmentToPostDetailsFragment(
                    it
                )
            )
        }
    }

    override fun renderView(savedInstanceState: Bundle?) {
        setUpRecycler()
    }

    private fun setUpRecycler() {
        rvAdapter = RecyclerViewAdapter(posts, object : RecyclerViewAdapter.OnPostSelectedListener {
            override fun onPostSelected(model: PostModel) {
                viewModel.goToDetails(model)
            }

        })
        val linearLayoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = rvAdapter
        recyclerView.layoutManager = linearLayoutManager
    }

}