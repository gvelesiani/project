package com.example.workpro.presentation.main

import com.example.workpro.domain.models.PostModel
import com.zuluft.mvvm.models.DisposableValue

data class MainFragmentViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val posts: DisposableValue<List<PostModel>>? = null,
    val savePostsToLocal: DisposableValue<Boolean>? = null,
    val dismissLoader: DisposableValue<Boolean>? = null,
    val goToDetails: DisposableValue<PostModel>? = null
)
