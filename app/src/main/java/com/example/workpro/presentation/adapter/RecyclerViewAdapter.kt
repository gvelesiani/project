package com.example.workpro.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.workpro.R
import com.example.workpro.domain.models.PostModel
import kotlinx.android.synthetic.main.item_layout.view.*

//class RecyclerViewAdapter(
//    private val posts: MutableList<PostModel>
//) :
//    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
//
//    var recyclerViewCallback: RecyclerViewCallback? = null
//
//    override fun onCreateViewHolder(
//        parent: ViewGroup,
//        viewType: Int
//    ): ViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
//        return ViewHolder(view)
//    }
//
//    fun setPosts(data: List<PostModel>){
//        posts.addAll(data)
//        notifyDataSetChanged()
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val model = posts[position]
//        holder.title.text = model.title
//        holder.body.text = model.description
//        holder.postId.text = model.id.toString()
//
//        holder.itemView.setOnClickListener {
//            this@RecyclerViewAdapter.recyclerViewCallback?.onRecycleViewItemClick(posts[position], position)
//        }
//
//    }
//
//    override fun getItemCount(): Int {
//        return posts.size
//    }
//
//    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
//        var title: TextView = v.postTitle
//        var body: TextView = v.postBody
//        var postId: TextView = v.postId
//    }
//
//    fun setOnCallbackListener(recyclerViewCallback: RecyclerViewCallback) {
//        this.recyclerViewCallback = recyclerViewCallback
//    }
//
//}

class RecyclerViewAdapter(
    private val posts: MutableList<PostModel>,
    private val onSelectedListener: OnPostSelectedListener
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        container: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(container.context!!)
        val itemView= inflater.inflate(R.layout.item_layout, container, false)
        val vh = ViewHolder(itemView)
        itemView.setOnClickListener {
            onSelectedListener.onPostSelected(posts[vh.adapterPosition])
        }
        return vh
    }
    fun setPosts(data: List<PostModel>){
        posts.addAll(data)
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = posts[position]
        holder.title.text = model.title
        holder.body.text = model.description
        holder.postId.text = model.id.toString()
    }
    override fun getItemCount(): Int {
        return posts.size
    }
    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var title: TextView = v.postTitle
        var body: TextView = v.postBody
        var postId: TextView = v.postId
    }
    interface OnPostSelectedListener {
        fun onPostSelected(model:PostModel)
    }
}