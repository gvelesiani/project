package com.example.workpro.presentation.postEdit

import com.example.workpro.domain.models.PostModel
import com.zuluft.mvvm.models.DisposableValue
import io.reactivex.disposables.Disposable

data class PostEditViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val goToMainFragment: DisposableValue<PostModel>? = null,
    val updateCurrentPost: DisposableValue<PostModel>? = null,
    val updatePostWithId: DisposableValue<Int>? = null,
    val updatedPost: DisposableValue<Boolean>? = null
)
