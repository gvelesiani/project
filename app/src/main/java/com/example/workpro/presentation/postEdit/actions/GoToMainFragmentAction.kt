package com.example.workpro.presentation.postEdit.actions

import com.example.workpro.domain.models.PostModel
import com.example.workpro.presentation.postEdit.PostEditViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue
import io.reactivex.disposables.Disposable

class GoToMainFragmentAction(private val post: PostModel) : ViewStateAction<PostEditViewState> {
    override fun newState(oldState: PostEditViewState): PostEditViewState {
        return oldState.copy(goToMainFragment = DisposableValue(post))
    }
}