package com.example.workpro.presentation.actions

import com.example.workpro.domain.models.PostModel
import com.example.workpro.presentation.main.MainFragmentViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class GetPostsAction(
    private val posts: List<PostModel>
) : ViewStateAction<MainFragmentViewState> {
    override fun newState(oldState: MainFragmentViewState): MainFragmentViewState {
        return oldState.copy(posts = DisposableValue(posts))
    }
}