package com.example.workpro.presentation.postEdit

import android.os.Bundle
import android.text.Editable
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import com.example.workpro.R
import com.example.workpro.domain.models.PostModel
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import kotlinx.android.synthetic.main.fragment_post_edit.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_post_edit)
class PostEditFragment : BaseFragment<PostEditViewState, PostEditViewModel>() {

    private val viewModel: PostEditViewModel by viewModel()
    override fun provideViewModel(): PostEditViewModel {
        return viewModel
    }

    override fun reflectState(viewState: PostEditViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            //requireContext().showThrowableMessage(it)
        }

        viewState.updatedPost?.getValue().notNull {
            Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show()
        }

        // Has some problems
//        viewState.updateCurrentPost?.getValue().notNull {
//            updatePost(it)
//        }
//        viewState.updatePostWithId?.getValue().notNull {
//            updatePost(it)
//        }
    }

    private fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)

    // write texts to editText-s
    override fun renderView(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        val args = PostEditFragmentArgs.fromBundle(requireArguments())

        args.currentPostEdit.title.notNull {
            postEditTitle.text.append(it)
        }
        args.currentPostEdit.description.notNull {
            postEditDescription.text.append(it)
        }

    }

    private fun updatePost(post: PostModel) {
        val model = PostModel(
            post.userId, post.id, postEditTitle.text.toString(), postEditDescription.text.toString()
        )

        viewModel.updatePost(model)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.submit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menu_submit) {
            //updatePost(PostEditFragmentArgs.fromBundle(requireArguments()).currentPostEdit.id)
            updatePost(PostEditFragmentArgs.fromBundle(requireArguments()).currentPostEdit)
//            Toast.makeText(
//                context,
//                PostEditFragmentArgs.fromBundle(requireArguments()).currentPostEdit.title,
//                Toast.LENGTH_SHORT
//            ).show()
            //findNavController().navigate(R.id.action_postEditFragment_to_mainFragment)
        }
        return super.onOptionsItemSelected(item)
    }
}