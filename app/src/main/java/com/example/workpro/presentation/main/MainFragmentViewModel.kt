package com.example.workpro.presentation.main

import com.example.workpro.domain.models.PostModel
import com.example.workpro.domain.useCases.FetchAndSavePostsUseCase
import com.example.workpro.domain.useCases.GetLocalPostsUseCase
import com.example.workpro.domain.useCases.GetPostsUseCase
import com.example.workpro.presentation.actions.DismissLoaderAction
import com.example.workpro.presentation.actions.GetPostsAction
import com.example.workpro.presentation.actions.ShowErrorAction
import com.example.workpro.presentation.actions.ShowLoaderAction
import com.example.workpro.presentation.main.actions.GoToDetailsAction
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.common.extensions.async
import com.zuluft.mvvm.viewModels.BaseViewModel

class MainFragmentViewModel(
    fetchAndSavePostsUseCase: FetchAndSavePostsUseCase,
    private val getLocalPostsUseCase: GetLocalPostsUseCase
) : BaseViewModel<MainFragmentViewState>() {

    override fun getInitialState(): MainFragmentViewState {
        return MainFragmentViewState()
    }

    init {
        registerDisposables(
            fetchAndSavePostsUseCase.start()
                .flatMap {
                    getLocalPostsUseCase.start()
                        .map {list->
                            GetPostsAction(list) as ViewStateAction<MainFragmentViewState> }
                }
                .onErrorReturn { ShowErrorAction(it) }
                .startWith(ShowLoaderAction())
                .async()
                .subscribe(this::dispatchAction)
        )
    }


    fun goToDetails(model: PostModel) {
        dispatchAction(GoToDetailsAction(model))
    }

}