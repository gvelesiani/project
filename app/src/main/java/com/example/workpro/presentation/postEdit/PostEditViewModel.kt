package com.example.workpro.presentation.postEdit

import com.example.workpro.domain.models.PostModel
import com.example.workpro.domain.repository.Repository
import com.example.workpro.domain.useCases.UpdatePostUseCase
import com.example.workpro.presentation.postEdit.actions.DrawUpdatedPostAction
import com.example.workpro.presentation.postEdit.actions.GoToMainFragmentAction
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.common.extensions.async
import com.zuluft.mvvm.viewModels.BaseViewModel

class PostEditViewModel(
    private val updatePostUseCase: UpdatePostUseCase
) : BaseViewModel<PostEditViewState>() {

    override fun getInitialState(): PostEditViewState {
        return PostEditViewState()
    }

    fun goToMainFragment(model: PostModel) {
        dispatchAction(GoToMainFragmentAction(model))
    }

    // Has some problems
    fun updatePost(post: PostModel) {
        registerDisposables(
            updatePostUseCase.start(post)
                .map {
                    DrawUpdatedPostAction() as ViewStateAction<PostEditViewState>
                }
                .async()
                .subscribe(this::dispatchAction)
        )
    }

    // new function with id
//    fun updatePostWithId(id: Int) {
//        repository.updateLocalPost(id)
//    }


}