package com.example.workpro.presentation.details

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.navigation.fragment.findNavController
import com.zuluft.mvvm.common.LayoutResId
import com.zuluft.mvvm.fragments.BaseFragment
import com.example.workpro.R
import com.zuluft.mvvm.common.extensions.notNull
import com.zuluft.mvvm.dialogs.Loader
import kotlinx.android.synthetic.main.fragment_post_details.*
import org.koin.android.viewmodel.ext.android.viewModel

@LayoutResId(R.layout.fragment_post_details)
class PostDetailsFragment : BaseFragment<PostDetailsViewState, PostDetailsViewModel>() {

    private val viewModel: PostDetailsViewModel by viewModel()

    override fun provideViewModel(): PostDetailsViewModel {
        return viewModel
    }

    override fun reflectState(viewState: PostDetailsViewState) {
        viewState.showLoader?.getValue().notNull {
            registerDialog(Loader.show(requireContext()))
        }

        viewState.showError?.getValue().notNull {
            dismissAndClearDialogs()
            //requireContext().showThrowableMessage(it)
        }

        viewState.goToEdit?.getValue().notNull {
            findNavController().navigate(
                PostDetailsFragmentDirections.actionPostDetailsFragmentToPostEditFragment(it)
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.edit_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.menu_edit){
            //Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show()
            viewModel.goToEditFragment(PostDetailsFragmentArgs.fromBundle(requireArguments()).currentPost)
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    override fun renderView(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        val args = PostDetailsFragmentArgs.fromBundle(requireArguments())
        detailTitle.text=args.currentPost.title
        detailDescription.text=args.currentPost.description
        detailUserId.text = "User Id : ${args.currentPost.userId}"
    }
}