package com.example.workpro.presentation.details.actions

import com.example.workpro.domain.models.PostModel
import com.example.workpro.presentation.details.PostDetailsViewState
import com.zuluft.mvvm.actions.ViewStateAction
import com.zuluft.mvvm.models.DisposableValue

class GoToPostEditAction(private val post: PostModel): ViewStateAction<PostDetailsViewState> {
    override fun newState(oldState: PostDetailsViewState): PostDetailsViewState {
        return oldState.copy(goToEdit = DisposableValue(post))
    }
}