package com.example.workpro.presentation.details

import com.example.workpro.domain.models.PostModel
import com.zuluft.mvvm.models.DisposableValue

data class PostDetailsViewState(
    val showLoader: DisposableValue<Boolean>? = null,
    val showError: DisposableValue<Throwable>? = null,
    val goTo: DisposableValue<Boolean>? = null,
    val goToEdit: DisposableValue<PostModel>? = null
)
