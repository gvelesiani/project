package com.example.workpro.app.modules

import com.example.workpro.constants.BASE_URL
import com.example.workpro.domain.dataProviders.global.GlobalDataProvider
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single {
        GsonBuilder()
            .create()
    }

    single {
        GsonConverterFactory.create(get())
    } bind Converter.Factory::class

    single {
        RxJava2CallAdapterFactory.create()
    } bind CallAdapter.Factory::class

    single {
        OkHttpClient.Builder()
            .build()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .addCallAdapterFactory(get())
            .baseUrl(BASE_URL)
            .addConverterFactory(get())
            .build()
    }

    single {
        get<Retrofit>().create(GlobalDataProvider::class.java)
    }
}