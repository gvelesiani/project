package com.example.workpro.app.modules

import androidx.room.Room
import com.example.workpro.domain.dataProviders.local.LocalDataProvider
import com.example.workpro.domain.dataProviders.local.LocalDataProviderImpl
import com.example.workpro.domain.dataProviders.local.Database
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.bind
import org.koin.dsl.module

val localStorageModule = module {

    single {
        Room.databaseBuilder(androidContext(), Database::class.java, "PostDb")
            .fallbackToDestructiveMigration().build()
    } bind Database::class

    single {
        LocalDataProviderImpl(get())
    } bind LocalDataProvider::class
}