package com.example.workpro.app

import android.app.Application
import com.example.workpro.app.modules.*
import com.zuluft.mvvm.app.MvvmApplication
import org.koin.core.module.Module

class App: MvvmApplication(){

    override fun onCreate() {
        super.onCreate()
    }
    override fun provideModules(): List<Module> {
        // Return Koin modules
        return listOf(
            localStorageModule,
            networkModule,
            repositoryModule,
            useCasesModule,
            mainViewModelModule
        )
    }

}