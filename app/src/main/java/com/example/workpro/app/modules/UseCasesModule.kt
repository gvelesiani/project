package com.example.workpro.app.modules

import com.example.workpro.domain.useCases.*
import org.koin.dsl.module

val useCasesModule = module {
    factory {
        GetPostsUseCase(get())
    }

    factory {
        SavePostsUseCase(get())
    }

    factory {
        FetchAndSavePostsUseCase(get(), get())
    }

    factory {
        GetLocalPostsUseCase(get())
    }

    factory {
        UpdatePostUseCase(get())
    }

}