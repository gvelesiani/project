package com.example.workpro.app.modules

import com.example.workpro.domain.repository.Repository
import com.example.workpro.domain.repository.RepositoryImpl
import org.koin.dsl.bind
import org.koin.dsl.module

val repositoryModule = module {
    single {
        RepositoryImpl(get(), get())
    } bind Repository::class
}