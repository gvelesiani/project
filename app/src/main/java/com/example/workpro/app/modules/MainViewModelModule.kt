package com.example.workpro.app.modules

import com.example.workpro.presentation.details.PostDetailsViewModel
import com.example.workpro.presentation.main.MainFragmentViewModel
import com.example.workpro.presentation.postEdit.PostEditViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val mainViewModelModule = module {
    viewModel {
        MainFragmentViewModel(get(), get())
    }
    viewModel {
        PostDetailsViewModel()
    }

    viewModel {
        PostEditViewModel(get())
    }
}