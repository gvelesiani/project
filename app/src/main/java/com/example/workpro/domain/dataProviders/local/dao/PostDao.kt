package com.example.workpro.domain.dataProviders.local.dao

import androidx.room.*
import com.example.workpro.domain.models.PostModel
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAllPosts(posts: List<PostModel>)

    @Query("SELECT * FROM posts")
    fun getLocalPosts(): Observable<List<PostModel>>

    @Query("DELETE FROM posts")
    abstract fun delete()

    @Query("UPDATE posts SET title=:title,description=:desc WHERE userId=:userId")
    abstract fun update(title: String, desc: String, userId: Int)

    @Transaction
    open fun updateAll(posts: List<PostModel>) {
        delete()
        return insertAllPosts(posts)
    }

}