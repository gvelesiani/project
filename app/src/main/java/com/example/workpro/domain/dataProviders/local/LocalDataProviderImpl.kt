package com.example.workpro.domain.dataProviders.local

import com.example.workpro.domain.models.PostModel
import io.reactivex.Observable

class LocalDataProviderImpl constructor(private val database: Database) : LocalDataProvider {
    override fun saveGlobalPostsToDatabase(posts: List<PostModel>): Observable<Boolean> {
        return Observable.fromCallable {
            database.getPostDao().updateAll(posts)
            true
        }
    }

    override fun getLocalPosts(): Observable<List<PostModel>> {
        return database.getPostDao().getLocalPosts()
    }

//    override fun updateLocalPost(id: Int): Completable {
//        return Completable.fromCallable {
//            database.getPostDao().update(id)
//        }
//    }

    // Works but has some problems
    override fun updatePost(post: PostModel): Observable<Boolean> {
        return Observable.fromCallable {
            database.getPostDao().update(post.title!!, post.description!!, post.userId)
            true
        }
    }


}