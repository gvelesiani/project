package com.example.workpro.domain.useCases

import com.example.workpro.domain.models.PostModel
import com.example.workpro.domain.repository.Repository
import io.reactivex.Observable

class GetLocalPostsUseCase (repository: Repository) : BaseRxUseCase<List<PostModel>, List<PostModel>>(repository){
    override fun start(arg: List<PostModel>?): Observable<List<PostModel>> {
        return repository.getLocalPosts()
    }
}