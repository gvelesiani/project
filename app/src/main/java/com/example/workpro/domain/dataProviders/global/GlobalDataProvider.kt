package com.example.workpro.domain.dataProviders.global

import com.example.workpro.domain.models.PostModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface GlobalDataProvider {
    @GET("/posts")
    fun getGlobalPosts() : Observable<List<PostModel>>

    @POST("/posts")
    fun editGlobalPost(@Body post: PostModel) : Response<PostModel>
}