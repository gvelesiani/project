package com.example.workpro.domain.repository

import com.example.workpro.domain.dataProviders.global.GlobalDataProvider
import com.example.workpro.domain.dataProviders.local.LocalDataProvider
import com.example.workpro.domain.models.PostModel
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Response

class RepositoryImpl constructor(
    private val globalDataProvider: GlobalDataProvider,
    private val localDataProvider: LocalDataProvider
) : Repository {

    override fun getGlobalPosts(): Observable<List<PostModel>> {
        return globalDataProvider.getGlobalPosts()
    }

    override fun getLocalPosts(): Observable<List<PostModel>> {
        return localDataProvider.getLocalPosts()
    }

    override fun saveGlobalPostsToDatabase(posts: List<PostModel>): Observable<Boolean> {
        return localDataProvider.saveGlobalPostsToDatabase(posts)
    }

//    override fun updateLocalPost(id: Int): Completable {
//        return localDataProvider.updatePost(id)
//    }

    // Has some problems
    override fun updatePost(post: PostModel): Observable<Boolean> {
        return localDataProvider.updatePost(post)
    }
}