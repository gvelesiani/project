package com.example.workpro.domain.useCases

import com.example.workpro.domain.models.PostModel
import com.example.workpro.domain.repository.Repository
import io.reactivex.Observable

class SavePostsUseCase(repository: Repository):
    BaseRxUseCase<List<PostModel>, Boolean>(repository) {
    override fun start(arg: List<PostModel>?): Observable<Boolean> {
        return repository.saveGlobalPostsToDatabase(arg!!)
    }
}