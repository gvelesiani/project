package com.example.workpro.domain.useCases

import com.example.workpro.domain.repository.Repository
import com.zuluft.mvvm.usecases.BaseUseCase
import io.reactivex.Observable

abstract class BaseRxUseCase<ARG_TYPE, RETURN_TYPE>(repository: Repository) :
    BaseUseCase<Repository, ARG_TYPE, Observable<RETURN_TYPE>>(repository)