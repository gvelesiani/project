package com.example.workpro.domain.dataProviders.local

import com.example.workpro.domain.models.PostModel
import io.reactivex.Completable
import io.reactivex.Observable

interface LocalDataProvider {
    fun saveGlobalPostsToDatabase(posts: List<PostModel>): Observable<Boolean>
    fun getLocalPosts(): Observable<List<PostModel>>

    // New
//    fun updateLocalPost(id: Int): Completable
    // Works but has some problems
    fun updatePost(post: PostModel): Observable<Boolean>
}