package com.example.workpro.domain.repository

import com.example.workpro.domain.models.PostModel
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.Response

interface Repository {
    fun getGlobalPosts(): Observable<List<PostModel>>
    fun getLocalPosts(): Observable<List<PostModel>>
    fun saveGlobalPostsToDatabase(posts: List<PostModel>) : Observable<Boolean>
//    fun updateLocalPost(id : Int): Completable

    // Works but has some problems
    fun updatePost(post: PostModel): Observable<Boolean>
}