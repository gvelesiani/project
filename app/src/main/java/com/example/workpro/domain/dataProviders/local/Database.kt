package com.example.workpro.domain.dataProviders.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.workpro.domain.dataProviders.local.dao.PostDao
import com.example.workpro.domain.models.PostModel

@Database(
    entities = [PostModel::class],
    exportSchema = false,
    version = 4
)
abstract class Database : RoomDatabase() {
    abstract fun getPostDao(): PostDao
}
