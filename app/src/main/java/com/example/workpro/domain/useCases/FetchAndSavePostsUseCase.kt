package com.example.workpro.domain.useCases

import com.example.workpro.domain.repository.Repository
import io.reactivex.Observable

class FetchAndSavePostsUseCase(
    repository: Repository,
    private val savePostsUseCase: SavePostsUseCase)
    : BaseRxUseCase<Unit, Boolean>(repository) {
    override fun start(arg: Unit?): Observable<Boolean> {
        return repository.getGlobalPosts()
            .flatMap {
                savePostsUseCase.start(it)
            }
    }
}