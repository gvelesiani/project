package com.example.workpro.domain.useCases

import com.example.workpro.domain.models.PostModel
import com.example.workpro.domain.repository.Repository
import io.reactivex.Observable

class UpdatePostUseCase(repository: Repository)
    : BaseRxUseCase<PostModel, Boolean>(repository) {
    override fun start(arg: PostModel?): Observable<Boolean> {
        return repository.updatePost(arg!!)
    }
}