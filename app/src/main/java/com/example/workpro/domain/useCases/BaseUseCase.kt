package com.example.workpro.domain.useCases

import com.example.workpro.domain.repository.Repository
import com.zuluft.mvvm.usecases.BaseUseCase


abstract class BaseUseCase<ARG_TYPE, RETURN_TYPE>(repository: Repository) :
    BaseUseCase<Repository, ARG_TYPE, RETURN_TYPE>(repository)